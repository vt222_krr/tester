from datetime import datetime
import os
import time
import string


def t1():
    with open("numbers.txt", "r") as file:
        nums = [int(line.strip()) for line in file]
    print(f"Sum of nums in file numbers.txt : {sum(nums)}")
    with open("sum_numbers.txt", "w") as file:
        file.write(str(sum(nums)))
        print("Sum was writed to sum_numbers.txt !")


def t2():
    nums = list(map(int, input("Enter the numbers with space : ").split()))
    with open("ispaired.txt", "w") as file:
        for elem in nums:
            if elem % 2 == 0:
                file.write(f"{elem} - paired\n")
            else:
                file.write(f"{elem} - unpaired\n")


def t3():
    with open("learning_python.txt", "r", encoding="utf-8") as file:
        lines = [line.strip() for line in file]
    print("\n".join(lines))
    print("lines was saved to file learning_python.txt")
    lines.sort(key=len)
    print("\n".join(lines))
    with open("learning_python.txt", "w", encoding="utf-8") as file:
        #sort by lines len
        for elem in lines:
            file.write(f"{elem}\n")


def t4():
    with open("learning_python.txt", "r", encoding="utf-8") as file:
        lines = [line.strip() for line in file]
    modifed_lines = [line.replace("Python", "Ruby") for line in lines]
    print("\n".join(modifed_lines))

    valid_phrases = []
    false_phrases = []

    for line in modifed_lines:
        print(f"Рядок: {line}")
        answer = input("Цей рядок актуальний для мови 'Ruby'? (так/ні): ")
        if answer.lower() == "так":
            valid_phrases.append(line)
        else:
            false_phrases.append(line)

    #valid
    with open("new_directory/modified_learning_python.txt", "w", encoding="utf-8") as val_file:
        val_file.write("\n".join(valid_phrases))
    #invalid
    with open("new_directory/invalid_phrases.txt", "w", encoding="utf-8") as inval_file:
        inval_file.write("\n".join(false_phrases))


def t5():
    current_datetime = datetime.now()
    if not os.path.exists("guest_book.txt") or os.path.getsize("guest_book.txt") == 0:
        with open("guest_book.txt", "w", encoding="utf-8") as file:
            file.write(f"Date of creating a file : {current_datetime.strftime('%d.%m.%Y / %H:%M')}\n")
            file.write(f"Date of last change : {current_datetime.strftime('%d.%m.%Y / %H:%M:%S')}\n")
    name = input("Enter ur name : ")
    welcome_msg = f"fell yourself like at your own home {name}"

    print(welcome_msg)
    current_datetime = datetime.now()

    with open("guest_book.txt", "a", encoding="utf-8") as update_file:
        with open("guest_book.txt", "r", encoding="utf-8") as file:
            lines = file.readlines()
        update_file.write(f"{welcome_msg} - {current_datetime.strftime('%d.%m.%Y / %H:%M:%S')}\n")
        lines[1] = f"date of last change : {current_datetime.strftime('%d.%m.%Y / %H:%M:%S')}\n"
        with open("guest_book.txt", "w", encoding="utf-8") as file:
            file.writelines(lines)


def t6():
    def letter_counter(text:str):
        letter_count = {}
        for char in text:
            if char.isalpha():
                char = char.lower()
                if char in letter_count:
                    letter_count[char] += 1
                else:
                    letter_count[char] = 1
        return letter_count

    def word_counter(text: str):
        word_count = {}
        words = text.split()
        for word in words:
            word = word.strip(string.punctuation).lower()
            if word in word_count:
                word_count[word] += 1
            else:
                word_count[word] = 1
        return word_count


    menu = int(input("1 - letter frequency\n2 - words frequency : "))
    with open("python_article.txt", "r", encoding="utf-8") as article_file:
        article_text = article_file.read()

    if menu == 1:
        current_datetime = time.strftime("%d.%m.%Y / %H:%M:%S")
        start_time = time.time()
        letter_count = letter_counter(article_text)
        end_time = time.time()
        execution_time = end_time - start_time
        with open("analysis_results.txt", "w", encoding="utf-8") as result_file:
            result_file.write(f"Analysis Results ({current_datetime})\n")
            result_file.write(f"Execution Time: {execution_time:.8f} seconds\n\n")
            result_file.write("Letter Frequency:\n")
            for letter, count in letter_count.items():
                result_file.write(f"{letter}: {count}\n")
    elif menu == 2:
        current_datetime = time.strftime("%d.%m.%Y / %H:%M:%S")
        start_time = time.time()
        word_count = word_counter(article_text)
        end_time = time.time()
        execution_time = end_time - start_time
        with open("analysis_results.txt", "w", encoding="utf-8") as result_file:
            result_file.write(f"Analysis Results ({current_datetime})\n")
            result_file.write(f"Execution Time: {execution_time:.12f} seconds\n\n")
            result_file.write("\nWord Frequency:\n")
            for word, count in word_count.items():
                result_file.write(f"{word}: {count}\n")

    print("Analysis complete. Results saved in analysis_results.txt")



# t1()
# t2()
# t3()
# t4()
# t5()
t6()
